public class Warm {
        public static boolean isCapital ( char c){
            return c >= 'A' && c <= 'Z';
        }

        public static boolean isSmall ( char c){
            return c >= 'a' && c <= 'z';
        }

        public static boolean isLetter ( char c){
            return isCapital(c) || isSmall(c);
        }

        public static boolean isVowel ( char c){
            final String vowels = "aeoiu";
            return vowels.indexOf(Character.toLowerCase(c)) >= 0;
        }

        public static boolean isConsonant ( char c){
            return !isVowel(c);
        }

}
