import java.util.Random;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        Scanner sc = new Scanner(System.in);
        //Filling array
        int c, d;
        char[][] array = new char[6][6];
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 5; j++) {
                array[i][j] = '-';
            }
        }
        //Generating random row and column
        Random rnd = new Random();
        int u = rnd.nextInt((5 - 1) + 1) + 1;
        int v = rnd.nextInt((5 - 1) + 1) + 1;
        //Game part
        do {
            System.out.print("Guess row: ");
            c = sc.nextInt();
            if (c > 5 || c <= 0) System.out.println("Enter only from 1 to 5 ");
            System.out.print("Guess column: ");
            d = sc.nextInt();
            if (d > 5 || d <= 0) System.out.println("Enter only from 1 to 5 ");
            array[u][v] = 'x';
            if (array[c][d] != array[u][v]) {
                array[c][d] = '*';
                for (int i = 1; i <= 5; i++) {
                    for (int j = 1; j <= 5; j++) {
                        if (array[i][j] == 'x') {
                            System.out.print("| - ");
                        } else
                            System.out.print("| " + array[i][j] + " ");
                    }
                    System.out.println();
                }
            } else {
                System.out.println("You have won");
                for (int i = 1; i <= 5; i++) {
                    for (int j = 1; j <= 5; j++) {
                        System.out.print("| " + array[i][j] + " ");
                    }
                    System.out.println();
                }
            }
        }
        while (array[c][d] != array[u][v]);
    }
}
